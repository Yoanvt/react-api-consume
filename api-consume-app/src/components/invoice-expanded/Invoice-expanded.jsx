import React from "react";
import axios from "axios";
import Invoice from "../invoice/Invoice";

class InvoiceExpanded extends React.Component {
  state = {
    invoice: null,
    error: false,
  };

  componentDidMount() {
    if (this.props.match.params.id) {
      axios
        .get(
          `https://protected-chamber-82435.herokuapp.com/invoices/` +
            this.props.match.params.id
        )
        .then((res) => {
          const invoice = res.data;
          this.setState({ invoice });
        })
        .catch((error) => {
          this.setState({ error: true });
        });
    }
  }

  onClickRedirectHandler = () => {
    this.props.history.push(`/editInvoice/${this.props.match.params.id}`);
  };

  onDeleteClickHandler = () => {
    if (this.props.match.params.id) {
      axios
        .delete(
          `https://protected-chamber-82435.herokuapp.com/invoices/` +
            this.props.match.params.id
        )
        .then((res) => {
          this.props.history.push("/");
        })
        .catch((error) => {
          this.setState({ error: true });
        });
    }
  };

  render() {
    let invoice = <span>Loading...</span>;
    if (this.state.error) {
      invoice = <span>Something went wrong!</span>;
    }
    if (this.state.invoice) {
      invoice = (
        <div style={{ textAlign: "center" }}>
          <Invoice invoice={this.state.invoice} disableRedirect={true} />
        </div>
      );
    }
    return (
      <div>
        <section className="Invoices">
          {invoice}
          <div>
            <button onClick={() => this.onClickRedirectHandler()}>Edit</button>
          </div>
          <div>
            <button onClick={() => this.onDeleteClickHandler()}>Delete</button>
          </div>
        </section>
      </div>
    );
  }
}

export default InvoiceExpanded;
