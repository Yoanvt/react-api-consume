import React from "react";
import "./InvoiceData.css";

const InvoiceData = (props) => {
  return (
    <div className="InvoiceData">
      <div>
        <p>Description: {props.invoiceData.description}</p>
        <p>Amount: {props.invoiceData.amount}</p>
        <p>Quantity: {props.invoiceData.qty}</p>
        <p>UnitPrice: {props.invoiceData.unitPrice}</p>
      </div>
    </div>
  );
};

export default InvoiceData;
