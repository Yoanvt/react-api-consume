import React from "react";
import "./Invoice.css";
import InvoiceData from "./InvoiceData/InvoiceData";
import { withRouter } from "react-router-dom";

const invoice = (props) => {
  const invoice = props.invoice;
  const invoiceData = props.invoice.invoiceData.map((invoiceData, index) => {
    return (
      <div key={props.invoice.id.toString() + index.toString()}>
        <hr></hr>
        <h4>Invoice data {index}</h4>
        <InvoiceData invoiceData={invoiceData} />
      </div>
    );
  });

  const onClickRedirectHandler = (id) => {
    if (props.disableRedirect) {
      return;
    }
    props.history.push("/invoiceExpanded/" + id);
  };

  return (
    <article
      className="Invoice"
      onClick={() => onClickRedirectHandler(invoice.id)}
    >
      <h1>Bill To:</h1>
      <div className="Info">
        <h4>{invoice.billTo.companyName}</h4>
        <div>ZIP: {invoice.billTo.ZIP}</div>
        <div>City: {invoice.billTo.city}</div>
        <div>CustomerId: {invoice.billTo.customerId}</div>
        <div>Email: {invoice.billTo.email}</div>
        <div>Name: {invoice.billTo.name}</div>
        <div>Phone: {invoice.billTo.phone}</div>
        <div>State: {invoice.billTo.state}</div>
        <div>StreetAddress: {invoice.billTo.streetAddress}</div>
        <div>Terms: {invoice.billTo.terms}</div>
      </div>
      <div>Date: {invoice.date}</div>
      <div>{invoiceData}</div>
    </article>
  );
};

export default withRouter(invoice);
