import React from "react";
import Invoice from "../invoice/Invoice";
import axios from "axios";
import { withRouter } from "react-router-dom";

import "./Invoices.css";

class Invoices extends React.Component {
  state = {
    invoices: [],
    search: "",
    sort: "true",
    error: false,
  };

  handleSearch(event) {
    this.setState({ search: event.target.value.substr(0, 20) });
  }

  handleSort(event) {
    this.setState({ sort: event.target.value });
  }

  onClickRedirectHandler = () => {
    this.props.history.push("/addInvoice");
  };

  componentDidMount() {
    axios
      .get(`https://protected-chamber-82435.herokuapp.com/invoices`)
      .then((res) => {
        const invoices = res.data;
        console.log(invoices);

        this.setState({ invoices });
      })
      .catch((error) => {
        this.setState({ error: true });
      });
  }

  render() {
    let invoices = <span>Something went worng!</span>;

    if (!this.state.error) {
      let flteredInvoices = this.state.invoices.filter((invoice) => {
        return (
          invoice.billTo.companyName
            .toLowerCase()
            .indexOf(this.state.search.toLowerCase()) !== -1
        );
        // if you can't fint this.state.search do not return
      });

      let sortedInvoices = flteredInvoices.sort((a, b) => {
        return this.state.sort === "true"
          ? new Date(b.date) - new Date(a.date)
          : new Date(a.date) - new Date(b.date);
      });

      invoices = sortedInvoices.map((invoice) => {
        return (
          <Invoice invoice={invoice} key={invoice.id} disableRedirect={false} />
        );
      });
    }

    return (
      <div>
        <div className="Search-Sort-Container">
          <div className="Search">
            <span>Search:</span>
            <input
              type="text"
              value={this.state.search}
              onChange={this.handleSearch.bind(this)}
            />
          </div>

          <div className="Sort">
            <span>Sort:</span>
            <select
              value={this.state.value}
              onChange={this.handleSort.bind(this)}
            >
              <option value="true">Ascending</option>
              <option value="false">Descending</option>
            </select>
          </div>

          <button onClick={() => this.onClickRedirectHandler()}>
            Add invoice
          </button>
        </div>

        <section className="Invoices">{invoices}</section>
      </div>
    );
  }
}

export default withRouter(Invoices);
