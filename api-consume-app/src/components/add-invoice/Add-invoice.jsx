import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";

import "./Add-invoice.css";

const formValid = ({ formErrors, submitted, isEditMode, ...rest }) => {
  let valid = true;

  Object.values(formErrors).forEach((val) => {
    if (Array.isArray(val)) {
      val.forEach((val) => {
        for (let prop in val) {
          val[prop].length > 0 && (valid = false);
        }
      });
    } else {
      val.length > 0 && (valid = false);
    }
  });

  Object.values(rest).forEach((val) => {
    if (Array.isArray(val)) {
      val.forEach((val) => {
        Object.values(val).forEach((val) => {
          val == null && (valid = false);
        });
      });
    } else {
      val == null && (valid = false);
    }
  });

  return valid;
};

class AddInvoice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ZIP: null,
      city: null,
      companyName: null,
      customerId: null,
      email: null,
      name: null,
      phone: null,
      state: null,
      streetAddress: null,
      terms: null,
      date: null,
      invoiceData: [
        {
          amount: null,
          description: null,
          qty: "",
          unitPrice: null,
        },
      ],
      submitted: false,
      isEditMode: false,
      formErrors: {
        ZIP: "",
        city: "",
        companyName: "",
        customerId: "",
        email: "",
        name: "",
        phone: "",
        state: "",
        streetAddress: "",
        terms: "",
        date: "",
        invoiceData: [
          {
            amount: "",
            description: "",
            // qty: "",
            unitPrice: "",
          },
        ],
      },
    };
  }

  componentDidMount() {
    if (this.props.match.params.id) {
      axios
        .get(
          `https://protected-chamber-82435.herokuapp.com/invoices/` +
            this.props.match.params.id
        )
        .then((res) => {
          const invoice = res.data;
          console.log(res.data);

          let newInvoiceDataFormErrors = invoice.invoiceData.map((data) => {
            return {
              amount: "",
              description: "",
              //   qty: "",
              unitPrice: "",
            };
          });
          this.setState({
            ZIP: res.data.billTo.ZIP,
            city: res.data.billTo.city,
            companyName: res.data.billTo.companyName,
            customerId: res.data.billTo.customerId,
            email: res.data.billTo.email,
            name: res.data.billTo.name,
            phone: res.data.billTo.phone,
            state: res.data.billTo.state,
            streetAddress: res.data.billTo.streetAddress,
            terms: res.data.billTo.terms,
            date: invoice.date,
            invoiceData: invoice.invoiceData,
            formErrors: {
              ...this.state.formErrors,
              invoiceData: newInvoiceDataFormErrors,
            },
            isEditMode: true,
          });
          console.log(this.state);
        })
        .catch((error) => {
          console.log("Error receiving data");
        });
    }
  }

  handleSubmit = (e) => {
    this.setState({ submitted: true });
    e.preventDefault();
    if (formValid(this.state)) {
      const data = {
        billTo: {
          ZIP: this.state.ZIP,
          city: this.state.city,
          companyName: this.state.companyName,
          customerId: +this.state.customerId,
          email: this.state.email,
          name: this.state.name,
          phone: this.state.phone,
          state: this.state.state,
          streetAddress: this.state.streetAddress,
          terms: this.state.terms,
        },
        date: new Date(this.state.date).toLocaleDateString("en-US"),
        invoiceData: this.state.invoiceData,
      };
      if (this.state.isEditMode) {
        console.log(data);

        axios
          .put(
            `https://protected-chamber-82435.herokuapp.com/invoices/` +
              this.props.match.params.id,
            data
          )
          .then((response) => {
            console.log(response);
            this.props.history.push("/");
          });
      } else {
        console.log(data);
        axios
          .post("https://protected-chamber-82435.herokuapp.com/invoices", data)
          .then((response) => {
            console.log(response);
            this.props.history.push("/");
          });
      }
    } else {
      console.error("Form is invalid");
    }
  };

  handleChange = (e) => {
    e.preventDefault();

    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };
    switch (name) {
      case "ZIP":
        formErrors.ZIP =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "city":
        formErrors.city =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "companyName":
        formErrors.companyName =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "customerId":
        formErrors.customerId =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "email":
        formErrors.email =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "name":
        formErrors.name =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "phone":
        formErrors.phone =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "state":
        formErrors.state =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "streetAddress":
        formErrors.streetAddress =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "terms":
        formErrors.terms =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;
      case "date":
        formErrors.date =
          value.length < 8 ? "minimum 8 characaters required" : "";
        break;
      default:
        break;
    }
    this.setState({ formErrors, [name]: value });
  };

  handleInnerChange = (e, index) => {
    e.preventDefault();
    let invoiceDataCopy = JSON.parse(JSON.stringify(this.state.invoiceData));
    let formErrors = { ...this.state.formErrors };
    const { name, value } = e.target;
    switch (name) {
      case "amount":
        formErrors.invoiceData[index].amount =
          value.length < 1 ? "minimum 1 characaters required" : "";
        invoiceDataCopy[index].amount = +value;

        break;
      case "description":
        formErrors.invoiceData[index].description =
          value.length < 3 ? "minimum 3 characaters required" : "";
        invoiceDataCopy[index].description = value;

        break;
      case "qty":
        invoiceDataCopy[index].qty = +value;

        break;
      case "unitPrice":
        formErrors.invoiceData[index].unitPrice =
          value.length < 1 ? "minimum 1 characaters required" : "";
        invoiceDataCopy[index].unitPrice = +value;
        break;
      default:
        break;
    }

    this.setState({ formErrors, invoiceData: invoiceDataCopy });
  };

  onAddInvoiceData() {
    let formErrorsCopy = JSON.parse(JSON.stringify(this.state.formErrors));
    formErrorsCopy.invoiceData.push({
      amount: "",
      description: "",
      unitPrice: "",
    });
    this.setState({
      invoiceData: [
        ...this.state.invoiceData,
        { amount: null, description: null, qty: "", unitPrice: null },
      ],
      formErrors: formErrorsCopy,
    });
  }

  onRemoveInvoiceData() {
    let formErrorsCopy = JSON.parse(JSON.stringify(this.state.formErrors));
    let invoiceDataCopy = JSON.parse(JSON.stringify(this.state.invoiceData));
    formErrorsCopy.invoiceData.pop();
    invoiceDataCopy.pop();

    this.setState({
      invoiceData: invoiceDataCopy,
      formErrors: formErrorsCopy,
    });
  }

  render() {
    let heading = <h1>Add invoice</h1>;
    if (this.props.match.params.id) {
      heading = <h1>Edit Invoice</h1>;
    }
    const { formErrors } = this.state;
    let invoiceData = <span>Loading...</span>;
    if (this.state.invoiceData) {
      invoiceData = this.state.invoiceData.map((invoiceData, index) => {
        let invoiceDatainputGroup = <span>Loading...</span>;
        if (this.state.invoiceData[index] && formErrors.invoiceData[index]) {
          invoiceDatainputGroup = (
            <div>
              <h4>InvoiceData {index}</h4>
              <div>
                <label htmlFor="amount">Amount</label>
                <input
                  placeholder="amount"
                  type="number"
                  name="amount"
                  noValidate
                  value={this.state.invoiceData[index].amount || ""}
                  onChange={(event) => this.handleInnerChange(event, index)}
                />
                {(!this.state.invoiceData[index].amount == null ||
                  !this.state.invoiceData[index].amount == "") &&
                  formErrors.invoiceData[index].amount.length > 0 && (
                    <div className="errorMessage">
                      {formErrors.invoiceData[index].amount}
                    </div>
                  )}
                {this.state.submitted &&
                  (this.state.invoiceData[index].amount == null ||
                    this.state.invoiceData[index].amount == "") && (
                    <div className="errorMessage">This field is required</div>
                  )}
              </div>
              <div>
                <label htmlFor="description">Description</label>
                <input
                  placeholder="description"
                  type="text"
                  name="description"
                  noValidate
                  value={this.state.invoiceData[index].description || ""}
                  onChange={(event) => this.handleInnerChange(event, index)}
                />
                {(!this.state.invoiceData[index].description == null ||
                  !this.state.invoiceData[index].description == "") &&
                  formErrors.invoiceData[index].description.length > 0 && (
                    <div className="errorMessage">
                      {formErrors.invoiceData[index].description}
                    </div>
                  )}
                {this.state.submitted &&
                  (this.state.invoiceData[index].description == null ||
                    this.state.invoiceData[index].description == "") && (
                    <div className="errorMessage">This field is required</div>
                  )}
              </div>
              <div>
                <label htmlFor="qty">Quantity</label>
                <input
                  placeholder="qty"
                  type="number"
                  name="qty"
                  noValidate
                  value={this.state.invoiceData[index].qty || ""}
                  onChange={(event) => this.handleInnerChange(event, index)}
                />
              </div>
              <div>
                <label htmlFor="unitPrice">Unit Price</label>
                <input
                  placeholder="unitPrice"
                  type="number"
                  name="unitPrice"
                  noValidate
                  value={this.state.invoiceData[index].unitPrice || ""}
                  onChange={(event) => this.handleInnerChange(event, index)}
                />
                {(!this.state.invoiceData[index].unitPrice == null ||
                  !this.state.invoiceData[index].unitPrice == "") &&
                  formErrors.invoiceData[index].unitPrice.length > 0 && (
                    <div className="errorMessage">
                      {formErrors.invoiceData[index].unitPrice}
                    </div>
                  )}
                {this.state.submitted &&
                  (this.state.invoiceData[index].unitPrice == null ||
                    this.state.invoiceData[index].unitPrice == "") && (
                    <div className="errorMessage">This field is required</div>
                  )}
              </div>
            </div>
          );
        }

        return <div key={index}>{invoiceDatainputGroup}</div>;
      });
    }

    return (
      <div className="NewPost">
        {heading}
        <form onSubmit={this.handleSubmit} noValidate>
          <div>
            <label htmlFor="ZIP1">ZIP</label>
            <input
              className={formErrors.ZIP.length > 0 ? "error" : null}
              placeholder="ZIP"
              type="text"
              name="ZIP"
              noValidate
              value={this.state.ZIP || ""}
              onChange={this.handleChange}
            />
            {(!this.state.ZIP == null || !this.state.ZIP == "") &&
              formErrors.ZIP.length > 0 && (
                <div className="errorMessage">{formErrors.ZIP}</div>
              )}
            {this.state.submitted &&
              (this.state.ZIP == null || this.state.ZIP == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="city">City</label>
            <input
              className={formErrors.city.length > 0 ? "error" : null}
              placeholder="city"
              type="text"
              name="city"
              noValidate
              value={this.state.city || ""}
              onChange={this.handleChange}
            />
            {(!this.state.city == null || !this.state.city == "") &&
              formErrors.city.length > 0 && (
                <div className="errorMessage">{formErrors.city}</div>
              )}
            {this.state.submitted &&
              (this.state.city == null || this.state.city == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="companyName">Company Name</label>
            <input
              className={formErrors.companyName.length > 0 ? "error" : null}
              placeholder="companyName"
              type="text"
              name="companyName"
              noValidate
              value={this.state.companyName || ""}
              onChange={this.handleChange}
            />
            {(!this.state.companyName == null ||
              !this.state.companyName == "") &&
              formErrors.companyName.length > 0 && (
                <div className="errorMessage">{formErrors.companyName}</div>
              )}
            {this.state.submitted &&
              (this.state.companyName == null ||
                this.state.companyName == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="customerId">Customer ID</label>
            <input
              className={formErrors.customerId.length > 0 ? "error" : null}
              placeholder="customerId"
              type="number"
              name="customerId"
              noValidate
              value={this.state.customerId || ""}
              onChange={this.handleChange}
            />
            {(!this.state.customerId == null || !this.state.customerId == "") &&
              formErrors.customerId.length > 0 && (
                <div className="errorMessage">{formErrors.customerId}</div>
              )}
            {this.state.submitted &&
              (this.state.customerId == null ||
                this.state.customerId == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="email">Email</label>
            <input
              className={formErrors.email.length > 0 ? "error" : null}
              placeholder="email"
              type="text"
              name="email"
              noValidate
              value={this.state.email || ""}
              onChange={this.handleChange}
            />
            {(!this.state.email == null || !this.state.email == "") &&
              formErrors.email.length > 0 && (
                <div className="errorMessage">{formErrors.email}</div>
              )}
            {this.state.submitted &&
              (this.state.email == null || this.state.email == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="name">Name</label>
            <input
              className={formErrors.name.length > 0 ? "error" : null}
              placeholder="name"
              type="text"
              name="name"
              noValidate
              value={this.state.name || ""}
              onChange={this.handleChange}
            />
            {(!this.state.name == null || !this.state.name == "") &&
              formErrors.name.length > 0 && (
                <div className="errorMessage">{formErrors.name}</div>
              )}
            {this.state.submitted &&
              (this.state.name == null || this.state.name == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="phone">Phone</label>
            <input
              className={formErrors.phone.length > 0 ? "error" : null}
              placeholder="phone"
              type="text"
              name="phone"
              noValidate
              value={this.state.phone || ""}
              onChange={this.handleChange}
            />
            {(!this.state.phone == null || !this.state.phone == "") &&
              formErrors.phone.length > 0 && (
                <div className="errorMessage">{formErrors.phone}</div>
              )}
            {this.state.submitted &&
              (this.state.phone == null || this.state.phone == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="state">State</label>
            <input
              className={formErrors.state.length > 0 ? "error" : null}
              placeholder="state"
              type="text"
              name="state"
              noValidate
              value={this.state.state || ""}
              onChange={this.handleChange}
            />
            {(!this.state.state == null || !this.state.state == "") &&
              formErrors.state.length > 0 && (
                <div className="errorMessage">{formErrors.state}</div>
              )}
            {this.state.submitted &&
              (this.state.state == null || this.state.state == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="streetAddress">Street Address</label>
            <input
              className={formErrors.streetAddress.length > 0 ? "error" : null}
              placeholder="streetAddress"
              type="text"
              name="streetAddress"
              noValidate
              value={this.state.streetAddress || ""}
              onChange={this.handleChange}
            />
            {(!this.state.streetAddress == null ||
              !this.state.streetAddress == "") &&
              formErrors.streetAddress.length > 0 && (
                <div className="errorMessage">{formErrors.streetAddress}</div>
              )}
            {this.state.submitted &&
              (this.state.streetAddress == null ||
                this.state.streetAddress == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="terms">Terms</label>
            <input
              className={formErrors.terms.length > 0 ? "error" : null}
              placeholder="terms"
              type="text"
              name="terms"
              noValidate
              value={this.state.terms || ""}
              onChange={this.handleChange}
            />
            {(!this.state.terms == null || !this.state.terms == "") &&
              formErrors.terms.length > 0 && (
                <div className="errorMessage">{formErrors.terms}</div>
              )}
            {this.state.submitted &&
              (this.state.terms == null || this.state.terms == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>
            <label htmlFor="date">Date</label>
            <input
              className={formErrors.date.length > 0 ? "error" : null}
              type="text"
              placeholder="mm/dd/yyyy"
              name="date"
              noValidate
              value={this.state.date || ""}
              onChange={this.handleChange}
            />
            {(!this.state.date == null || !this.state.date == "") &&
              formErrors.date.length > 0 && (
                <div className="errorMessage">{formErrors.date}</div>
              )}
            {this.state.submitted &&
              (this.state.date == null || this.state.date == "") && (
                <div className="errorMessage">This field is required</div>
              )}
          </div>
          <div>{invoiceData}</div>
          <div>
            <button type="button" onClick={() => this.onAddInvoiceData()}>
              + Add InvoiceData
            </button>
            <button type="button" onClick={() => this.onRemoveInvoiceData()}>
              - Remove last InvoiceData
            </button>
          </div>
          <button type="submit">Submit Invoice</button>
        </form>
      </div>
    );
  }
}

export default withRouter(AddInvoice);
