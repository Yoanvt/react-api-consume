import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import "./App.css";
import Invoices from "./components/invoices/Invoices";
import InvoiceExpanded from "./components/invoice-expanded/Invoice-expanded";
import AddInvoice from "./components/add-invoice/Add-invoice";

function App() {
  let routes = (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Invoices} />
        <Route path="/addInvoice" exact component={AddInvoice} />
        <Route path="/editInvoice/:id" exact component={AddInvoice} />
        <Route path="/invoiceExpanded/:id" exact component={InvoiceExpanded} />
        {/* add more routes here */}
      </Switch>
    </BrowserRouter>
  );

  return <div className="App">{routes}</div>;
}

export default App;
